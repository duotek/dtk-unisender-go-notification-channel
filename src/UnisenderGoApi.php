<?php

namespace Duotek\UnisenderGoNotificationChannel;

use Duotek\UnisenderGoNotificationChannel\Exceptions\UnisenderGoException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\GuzzleException;
use Throwable;

/**
 * Unisender Go. WebAPI v1.52
 * @see https://godocs.unisender.ru/web-api-ref#web-api
 */
class UnisenderGoApi
{
    protected string $baseUrl = 'https://go1.unisender.ru/ru/transactional/api/v1/';
    protected string|null $token = null;

    public function __construct(string|null $token = null)
    {
        $this->token = $token;
    }

    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    public function setBaseUrl(string $baseUrl): void
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * Ограничения:
     * — максимальный размер запроса - 10 MB
     * — максимальное количество получателей – 500 email
     * — В названии вложений запрещен символ '/'
     */
    public function sendEmail(UnisenderGoMessage $message): void
    {
        $requestBody = [
            'message' => [
                'recipients' => [],
            ],
        ];

        $requestBody['message']['recipients'] = $message->getRecipients();

        if ($message->getGlobalSubstitutions()) {
            $requestBody['message']['global_substitutions'] = $message->getGlobalSubstitutions();
        }

        if ($message->getTemplateId()) {
            $requestBody['message']['template_id'] = $message->getTemplateId();
        }

        if ($message->getBody()) {
            $requestBody['message']['body'] = $message->getBody();
        }

        if ($message->getSubject()) {
            $requestBody['message']['subject'] = $message->getSubject();
        }

        if ($message->getGlobalSubstitutions()) {
            $requestBody['message']['global_substitutions'] = $message->getGlobalSubstitutions();
        }

        if ($message->getGlobalLanguage()) {
            $requestBody['message']['global_language'] = $message->getGlobalLanguage();
        }

        if ($message->getSkipUnsubscribe()) {
            $requestBody['message']['skip_unsubscribe'] = $message->getSkipUnsubscribe();
        }

        if ($message->getSubject()) {
            $requestBody['message']['subject'] = $message->getSubject();
        }

        if ($message->getTags()) {
            $requestBody['message']['tags'] = $message->getTags();
        }

        if ($message->getAttachments()) {
            $requestBody['message']['attachments'] = $message->getAttachments();
        }

        if (!isset($requestBody['message']['body']) && !isset($requestBody['message']['template_id'])) {
            throw new UnisenderGoException('You must specify either body or template_id param');
        }

        if ($message->getFromName()) {
            $requestBody['message']['from_name'] = $message->getFromName();
        }

        if (!$message->getFromEmail()) {
            throw new UnisenderGoException('You must specify fromEmail param');
        }

        $requestBody['message']['from_email'] = $message->getFromEmail();

        $this->request('email/send.json', $requestBody);
    }

    /**
     * @throws GuzzleException
     * @throws Throwable
     */
    private function request(string $uri, array $body, array $headers = []): void
    {
        $headers = array_merge($headers, [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'X-API-KEY' => $this->token,
        ]);

        $client = new Client([
            'base_uri' => $this->baseUrl,
            'verify' => false, // Позволяет обойти ошибку в сертификате у Unisender Go (используют Let's encrypt)
        ]);

        try {
            $client->request('POST', $uri, [
                'headers' => $headers,
                'json' => $body,
            ]);
        } catch (BadResponseException $e) {

            $message = match ($e->getResponse()->getStatusCode()) {
                400 => 'Запрос имеет ошибки, проверьте формат запроса и состав параметров',
                401 => 'Неверный API-ключ',
                403 => 'У вас не хватает прав',
                404 => 'Не найден указанный метод',
                413 => 'Размер запроса превышает 10 МБ',
                429 => 'Слишком много запросов, уменьшите их частоту'
            };

            $responseBody = $e->getResponse()->getBody()->getContents();

            throw new UnisenderGoException("{$message}. {$responseBody}");

        } catch (Throwable $e) {
            throw new $e;
        }
    }
}
