<?php

namespace Duotek\UnisenderGoNotificationChannel;

use BadMethodCallException;
use Illuminate\Notifications\Notification;
use InvalidArgumentException;

class UnisenderGoChannel
{
    protected UnisenderGoApi $api;

    public function __construct(UnisenderGoApi $api)
    {
        $this->api = $api;
    }

    public function send($notifiable, Notification $notification): void
    {
        if (!method_exists($notification, 'toUnisenderGo')) {
            throw new BadMethodCallException('Method "toGoUnisender" does not exists on given notification instance.');
        }

        /** @var UnisenderGoMessage $message */
        $message = $notification->toUnisenderGo($notifiable);

        if (!($message instanceof UnisenderGoMessage)) {
            throw new InvalidArgumentException('Message is not an instance of UnisenderGoMessage.');
        }

        if ($message->getRecipients()) {
            $this->api->sendEmail($message);
            return;
        }

        $to = $notifiable->routeNotificationForUnisenderGo($notification);

        if (!$to) {
            throw new InvalidArgumentException('Receivers not specified');
        }

        $message->addRecipient($to[0], isset($to[1]) ?? [], isset($to[2]) ?? []);

        $this->api->sendEmail($message);
    }
}
