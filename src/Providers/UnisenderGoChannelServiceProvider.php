<?php

namespace Duotek\UnisenderGoNotificationChannel\Providers;

use Illuminate\Support\ServiceProvider;
use Duotek\UnisenderGoNotificationChannel\UnisenderGoApi;

class UnisenderGoChannelServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singletonIf(UnisenderGoApi::class, function () {
            $unisenderGoApi = new UnisenderGoApi(config('services.unisender_go.api_key'));

            if ($baseUrl = config('services.unisender_go.base_url')) {
                $unisenderGoApi->setBaseUrl($baseUrl);
            }

            return $unisenderGoApi;
        });
    }
}
