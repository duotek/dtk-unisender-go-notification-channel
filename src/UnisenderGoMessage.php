<?php

namespace Duotek\UnisenderGoNotificationChannel;

use Duotek\UnisenderGoNotificationChannel\Exceptions\UnisenderGoException;
use Illuminate\Contracts\View\View;

class UnisenderGoMessage
{
    /**
     * Список получателей (с подстановками и метаданными опционально)
     */
    private array $recipients = [];

    /**
     * Глобальные подстановки.
     *
     * Предупрежнение:
     * Персональные подстановки имееют приоритет перед глобальными
     * и в случае совпадения будут перезаписаны
     */
    private array $globalSubstitutions = [];

    /**
     * Cодержит в себе html, plaintext и amp.
     * (При указанном templateId указывать необязательно)
     */
    private array $body = [];

    /**
     * От какого email
     **/
    private string|null $fromEmail;

    /**
     * От какого имени
     **/
    private string|null $fromName;

    /**
     * Тема письма.
     */
    private string $subject = 'Без темы';

    /**
     * Уникальный идентификатор шаблона.
     * (При указанном body указывать необязательно)
     */
    private string|null $templateId = null;

    /**
     * Заголовок для выбора языка ссылки и страницы отписки.
     * Допустимые значения “be”, “de”, “en”, “es”, “fr”, “it”, “pl”, “pt”, “ru”, “ua”.
     */
    private string $globalLanguage = 'ru';

    /**
     * Пропустить или не пропускать добавление стандартного блока
     * со ссылкой отписки к HTML-части письма.
     */
    private int $skipUnsubscribe = 0;

    /**
     * Метки могут использоваться для категоризации писем по выбранным вами критериям
     */
    private array $tags = [];

    /**
     * Вложения к письму
     * (Максимальный размер файла в одном вложении 7МБ)
     */
    private array $attachments = [];

    public function __construct()
    {
        $this->fromEmail = config('mail.from.address');
        $this->fromName = config('mail.from.name');
    }

    /**
     * Добавить получателя
     */
    public function addRecipient(string $email, $substitutions = [], $metadata = []): static
    {
        $recipient = ['email' => $email];

        if ($substitutions) {
            $recipient['substitutions'] = $substitutions;
        }

        if ($metadata) {
            $recipient['metadata'] = $metadata;
        }

        array_push($this->recipients, $recipient);

        return $this;
    }

    /**
     * Установить много получателей с возможностью указать подстановки и метадату
     *
     * @param array $recipientsData [['user@mail.ru', ['substitutionParam1' => 'substitutionValue1'], ['metadataParam1' => 'metadataValue1']]]
     *
     * Более подробно: @see https://gitlab.com/duotek/dtk-unisender-go-notification-channel
     */
    public function setRecipients(array $recipientsData): static
    {
        if (!$recipientsData) {
            throw new UnisenderGoException('Recipient list is empty');
        }

        $this->recipients = [];

        foreach ($recipientsData as $recipientData) {

            $recipient = [
                'email' => $recipientData[0]
            ];

            if (isset($recipientData[1])) {
                $recipient['substitutions'] = $recipientData[1];
            }

            if (isset($recipientData[2])) {
                $recipient['metadata'] = $recipientData[2];
            }

            array_push($this->recipients, $recipient);
        }

        return $this;
    }

    /**
     * Установить тело письма
     */
    public function setBody(string|View $content, string $type = 'html'): static
    {
        if ($content instanceof View) {
            $content = $content->render();
        }

        if (!in_array($type, ['html', 'plaintext', 'amp'])) {
            throw new UnisenderGoException('Wrong type of body');
        }

        $this->body = [$type => $content];

        return $this;
    }

    public function setFromEmail(string $fromEmail): static
    {
        $this->fromEmail = $fromEmail;

        return $this;
    }

    public function setFromName(string $fromName): static
    {
        $this->fromName = $fromName;

        return $this;
    }

    /**
     * Установка глобальных подстановок
     */
    public function setGlobalSubstitutions(array $globalSubstitutions): static
    {
        $this->globalSubstitutions = $globalSubstitutions;

        return $this;
    }

    /**
     * Установка идентификатора шаблона
     */
    public function setTemplateId(string $templateId): static
    {
        $this->templateId = $templateId;

        return $this;
    }

    public function setSubject(string $subject): static
    {
        $this->subject = $subject;

        return $this;
    }

    public function setGlobalLanguage(string $lang): static
    {
        if (!in_array($lang, ['be', 'de', 'en', 'es', 'fr', 'it', 'pl', 'pt', 'ru', 'ua'])) {
            throw new UnisenderGoException('Указан недопустимый язык');
        }

        $this->globalLanguage = $lang;

        return $this;
    }

    public function skipUnsubscribe(): static
    {
        $this->skipUnsubscribe = 1;

        return $this;
    }

    public function setTags($tags = []): static
    {
        $this->tags = $tags;

        return $this;
    }

    public function addAttachment($nameWithExtension, $content, $type = 'application/octet-stream'): static
    {
        $this->attachments[] = [
            'type' => $type,
            'name' => $nameWithExtension,
            'content' => $content
        ];

        return $this;
    }

    public function getRecipients(): array
    {
        return $this->recipients;
    }

    public function getGlobalSubstitutions(): array
    {
        return $this->globalSubstitutions;
    }

    public function getBody(): array
    {
        return $this->body;
    }

    public function getFromEmail(): string
    {
        return $this->fromEmail;
    }

    public function getFromName(): string
    {
        return $this->fromName;
    }

    public function getTemplateId(): string|null
    {
        return $this->templateId;
    }

    public function getGlobalLanguage(): string
    {
        return $this->globalLanguage;
    }

    public function getSkipUnsubscribe(): ?int
    {
        return $this->skipUnsubscribe;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function getTags(): array
    {
        return $this->tags;
    }

    public function getAttachments(): array
    {
        return $this->attachments;
    }
}
