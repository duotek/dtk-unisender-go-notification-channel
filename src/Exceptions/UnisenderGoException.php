<?php

namespace Duotek\UnisenderGoNotificationChannel\Exceptions;

use RuntimeException;

class UnisenderGoException extends RuntimeException
{
}
